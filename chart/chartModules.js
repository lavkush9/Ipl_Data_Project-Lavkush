function loadDoc(fileName) {
    let data = [];
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            data = JSON.parse(this.responseText);
        }
    };
    xhttp.open("GET", fileName, false);
    xhttp.send();
    return data;
}

function matchPerSeason() {
    let matchPerSeason = loadDoc("../jsons-files/matchesPerSessionInAllYear.json");
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Ipl Matches Per Year'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number Of Matches'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Number Of Matches Per Year: <b>{point.y:1f}</b>'
        },
        series: [{
            name: 'Population',
            data: matchPerSeason,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}
matchPerSeason();

function teamsWinsmatchesPerYear() {
    let teamsWin = loadDoc("../jsons-files/teamWinPerYear.json");
    let years = loadDoc("../Json-file/teamPerYear.json");
    Highcharts.chart('containerbar', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Stacked bar chart for Match Wins Per Team'
        },
        xAxis: {
            categories: years
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Match Wins'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: teamsWin,
    });
}
teamsWinsmatchesPerYear()

function extraPerTeam() {
    let extraPerTeam = loadDoc("../jsons-files/extraRunPerTeam.json");

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Extra Runs Per Team In 2016"
        },
        axisY: {
            title: "Runs Per Team(2016)"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            dataPoints: extraPerTeam,
        }]
    });
    chart.render();
}
extraPerTeam();

function bowlerEconmyRate() {
    let bowlerEconomyRate = loadDoc("../jsons-files/bowlersEconomyRates.json");
    var chart = new CanvasJS.Chart("chartContainer1", {
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Economy Rate Of Bowlers"
        },
        axisY: {
            title: "Economy Rate"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            dataPoints: bowlerEconomyRate,
        }]
    });
    chart.render();
}
bowlerEconmyRate();


function topTenWicketTaker() {
    let topTen = loadDoc("../jsons-files/topTenBowlersInAllYear.json");
    var chart = new CanvasJS.Chart("chartContainer2", {
        theme: "light1", // "light1", "light2", "dark1", "dark2"
        title: {
            text: "Top wicket Taker in all Years"
        },
        axisY: {
            title: "Wickets"
        },
        data: [{
            type: "column",
            legendMarkerColor: "grey",
            dataPoints: topTen,
        }]
    });
    chart.render();
}
topTenWicketTaker();