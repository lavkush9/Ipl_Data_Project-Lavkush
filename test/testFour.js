const expect = require('chai').expect
const path = require('path')
const matchDataset = path.resolve("../csv-files/matchesTest.csv")
const deliveriesDataset = path.resolve("../csv-files/deliveriesTestFile.csv")
const methodCall = require('../src/fourth');
describe('Question Fourth Test', function () {
    it('file path should be correct', async function () {
        const expectResult = false;
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset, matchDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('function should not be reterns undefined ', async function () {
        const expectResult = undefined;
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset, matchDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('data file should have data', async function () {
        const expectResult = {};
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset, matchDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                "label": "UT Yadav",
                "y": 2
            },
            {
                "label": "SP Narine",
                "y": 4
            },
            {
                "label": "CH Morris",
                "y": 5
            },
            {
                "label": "RA Jadeja",
                "y": 6
            },
            {
                "label": "GB Hogg",
                "y": 6
            },
            {
                "label": "SR Watson",
                "y": 6
            },
            {
                "label": "MA Starc",
                "y": 6
            },
            {
                "label": "HV Patel",
                "y": 6
            },
            {
                "label": "AD Russell",
                "y": 6
            },
            {
                "label": "SL Malinga",
                "y": 6
            }
        ];
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset, matchDataset);
        expect(result).to.deep.equal(expectResult);
    })
})