const expect = require('chai').expect
const path = require('path')
const matchDataset = path.resolve("../csv-files/matchesTest.csv")
const deliveriesDataset = path.resolve("../csv-files/deliveriesTestFile.csv")
const methodCall = require('../src/third');
describe('Question Third Test', function () {
    it('file path should be correct', async function () {
        const expectResult = false;
        let result = await methodCall.getWiningListPerYear(matchDataset, deliveriesDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('function should not be reterns undefined ', async function () {
        const expectResult = undefined;
        let result = await methodCall.getWiningListPerYear(matchDataset, deliveriesDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('data file should have data', async function () {
        const expectResult = {};
        let result = await methodCall.getWiningListPerYear(matchDataset, deliveriesDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                "label": "Delhi Daredevils",
                "y": 13
            },
            {
                "label": "Sunrisers Hyderabad",
                "y": 33
            },
            {
                "label": "Rising Pune Supergiants",
                "y": 1
            },
            {
                "label": "Kings XI Punjab",
                "y": 0
            },
            {
                "label": "Gujarat Lions",
                "y": 24
            },
            {
                "label": "Mumbai Indians",
                "y": 3
            },
            {
                "label": "Kolkata Knight Riders",
                "y": 17
            },
            {
                "label": "Royal Challengers Bangalore",
                "y": 31
            }
        ];
        let result = await methodCall.getWiningListPerYear(matchDataset, deliveriesDataset);
        expect(result).to.deep.equal(expectResult);
    })

})