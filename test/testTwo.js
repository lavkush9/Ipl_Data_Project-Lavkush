const expect = require('chai').expect
const path = require('path')
const matchDataset = path.resolve("../csv-files/matchesTest2.csv")
const methodCall = require('../src/second');
describe('Question Second Test', function () {
    it('file path should be correct', async function () {
        const expectResult = false;
        let result = await methodCall.getWiningListPerYear(matchDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('function should not be reterns undefined ', async function () {
        const expectResult = undefined;
        let result = await methodCall.getWiningListPerYear(matchDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('data file should have data', async function () {
        const expectResult = {};
        let result = await methodCall.getWiningListPerYear(matchDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                name: 'Sunrisers Hyderabad',
                data: [0, 1]
            },
            {
                name: 'Delhi Daredevils',
                data: [1, 0]
            }
        ]
        let result = await methodCall.getWiningListPerYear(matchDataset);
        expect(result).to.deep.equal(expectResult);
    })
})