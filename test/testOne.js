const expect = require('chai').expect
const path = require('path')
const matchDataSet = path.resolve("../csv-files/matchesTest2.csv")
const fileName = path.resolve("../src/first.js")
const methodeCall = require(fileName)
describe('question One Test', function () {
    it('file path should be correct', async function () {
        const expectResult = false;
        let result = await methodeCall.getTotalMatchPerYear(matchDataSet);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('function should not be reterns undefined ', async function () {
        const expectResult = undefined;
        let result = await methodeCall.getTotalMatchPerYear(matchDataSet);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('data file should have data', async function () {
        const expectResult = {};
        let result = await methodeCall.getTotalMatchPerYear(matchDataSet);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [
            [2008, 1],
            [2017, 1]
        ];
        let result = await methodeCall.getTotalMatchPerYear(matchDataSet);
        expect(result).to.deep.equal(expectResult);
    })

})