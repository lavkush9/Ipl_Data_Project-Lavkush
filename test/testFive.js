const expect = require('chai').expect
const path = require('path')
const deliveriesDataset = path.resolve("../csv-files/deliveriesTestFile.csv")
const methodCall = require('../src/fifth');
describe('Question Five Test', function () {
    it('file path should be correct', async function () {
        const expectResult = false;
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('function should not be reterns undefined ', async function () {
        const expectResult = undefined;
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('data file should have data', async function () {
        const expectResult = {};
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset);
        expect(result).to.deep.not.equal(expectResult);
    })

    it('fetched data should be match', async function () {
        const expectResult = [{
                y: 10,
                label: 'DS Kulkarni'
            },
            {
                y: 8,
                label: 'B Kumar'
            },
            {
                y: 7,
                label: 'CJ Jordan'
            },
            {
                y: 6,
                label: 'SR Watson'
            },
            {
                y: 6,
                label: 'DJ Bravo'
            },
            {
                y: 5,
                label: 'MC Henriques'
            },
            {
                y: 5,
                label: 'BB Sran'
            },
            {
                y: 5,
                label: 'S Kaushik'
            },
            {
                y: 5,
                label: 'BCJ Cutting'
            },
            {
                y: 5,
                label: 'YS Chahal'
            }
        ];
        let result = await methodCall.getEconomyratePerBowler(deliveriesDataset);
        expect(result).to.deep.equal(expectResult);
    })
})