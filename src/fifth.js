var fs = require('fs');
var csv = require('fast-csv');

function getEconomyratePerBowler(f1) {
    let listOfBowler = [];
    let bowlerObjects = [];
    let obj = {};
    let bowlers = [];
    return new Promise(function (resolve, reject) {
        fs.createReadStream(f1).pipe(csv()).on('data', function (rawData) {
            let matchId = rawData[0];
            let wicket = rawData[18];
            everyBowlerObject = {};
            if (wicket != '' && matchId != 'match_id') {
                bowlers.push(rawData[8]);
                everyBowlerObject["matchId"] = rawData[0];
                everyBowlerObject["bowlerName"] = rawData[8];
                bowlerObjects.push(everyBowlerObject);
            }
        }).on('end', function () {
            bowlersName = [...new Set(bowlers)];
            // let tempCount = 0;
            for (let index = 0; index < bowlersName.length; index++) {
                let temp = bowlersName[index];
                let count = 0;
                wicketObject = {};
                for (let i = 0; i < bowlerObjects.length; i++) {
                    if (temp == bowlerObjects[i].bowlerName) {
                        count++;
                    }
                }
                wicketObject["y"] = count;
                wicketObject["label"] = temp;
                listOfBowler.push(wicketObject);
                // tempCount++;
            }
            let shortValues = listOfBowler.sort(function (a, b) {
                return b.y - a.y;
            })
            let topTen = [];
            for (let i = 0; i < 10; i++) {
                topTen.push(shortValues[i]);
            }
            // fs.writeFile("../jsons-files/topTenBowlersInAllYear.json", JSON.stringify(topTen, null, 9), (err) => {
            //     if (err) {
            //         console.log("json not craeted");
            //         return;
            //     }
            // });
            resolve(topTen);
        })
    })

}
getEconomyratePerBowler('../csv-files/deliveries.csv');
module.exports = {
    getEconomyratePerBowler: getEconomyratePerBowler,
}