function getTotalMatchPerYear(fileName) {
    var fs = require('fs');
    var csv = require('fast-csv');
    var matchYears = [];
    return new Promise(function (resolve, reject) {
        fs.createReadStream(fileName).pipe(csv()).on('data', function (rawData) {
            let yearOfMatch = Number(rawData[1]);
            if (yearOfMatch) {
                matchYears.push(yearOfMatch);
            }
        }).on('end', function () {
            let years = [...new Set(matchYears)].sort(function (a, b) {
                return a - b
            });
            let matchesPerYear = [];
            for (let i = 0; i < years.length; i++) {
                let count = 0;
                let tempYear = years[i];
                let teamObject = [];
                for (let index = 0; index < matchYears.length; index++) {
                    if (tempYear == matchYears[index]) {
                        count++;
                    }
                }
                teamObject.push(tempYear);
                teamObject.push(count);
                matchesPerYear.push(teamObject);
            }
            resolve(matchesPerYear);
        });
    })
}
getTotalMatchPerYear('../csv-files/matches.csv');

module.exports = {
    getTotalMatchPerYear: getTotalMatchPerYear,
}