function getEconomyratePerBowler(f1, f2) {
    var fs = require('fs');
    var csv = require('fast-csv');
    var matchesId = [];
    let listOfBowler = [];
    let bowlerObjects = [];
    let bowlersName;
    let objectOfBowlerEconomy;
    return new Promise(function (resolve, reject) {
        fs.createReadStream(f2).pipe(csv()).on('data', function (rawData) {
            let matchId = rawData[0];
            let year = rawData[1];
            if (year == 2015) {
                matchesId.push(matchId);
            }
        }).on('end', function () {

            fs.createReadStream(f1).pipe(csv()).on('data', function (rawData) {
                let everyBowlerObject = {};
                if (matchesId.includes(rawData[0])) {
                    listOfBowler.push(rawData[8]);
                    everyBowlerObject["bowler"] = rawData[8];
                    everyBowlerObject["wide_runs"] = Number(rawData[10]);
                    everyBowlerObject["noball_runs"] = Number(rawData[13]);
                    everyBowlerObject["total_runs"] = Number(rawData[17]);
                    bowlerObjects.push(everyBowlerObject);
                }
            }).on('end', function () {
                bowlersName = [...new Set(listOfBowler)];
                objectOfBowlerEconomy = calculateEconomy(bowlersName, bowlerObjects);
                resolve(objectOfBowlerEconomy);
            })
        })

    })
}
getEconomyratePerBowler('../csv-files/deliveries.csv', '../csv-files/matches.csv');


function calculateEconomy(bowlersName, bowlerObjects) {
    let objectOfBowlerEconomy = [];
    for (let k = 0; k < bowlersName.length; k++) {
        let perBowlerObject = {};
        nameOfBowler = bowlersName[k];
        let ball = 0;
        let runs = 0;
        for (let i = 0; i < bowlerObjects.length; i++) {
            if (nameOfBowler == bowlerObjects[i].bowler) {
                runs += bowlerObjects[i].total_runs;
                if (bowlerObjects[i].wide_runs == 0 && bowlerObjects[i].noball_runs == 0) {
                    ball++;
                }
            }
        }
        perBowlerObject["y"] = Number(((runs * 6) / ball).toFixed(2));
        perBowlerObject["label"] = nameOfBowler;
        objectOfBowlerEconomy.push(perBowlerObject);
    }
    let tempObj = objectOfBowlerEconomy.sort(function (fileOne, fileSecond) {
        return fileOne.y - fileSecond.y;
    });
    let sortData = [];
    for (let j = 0; j < 10; j++) {
        sortData.push(tempObj[j]);
    }
    return sortData;
}

module.exports = {
    getEconomyratePerBowler: getEconomyratePerBowler,
}