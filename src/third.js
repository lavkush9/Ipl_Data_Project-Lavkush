function getWiningListPerYear(matchFile, deliveriesFileName) {
    var fs = require('fs');
    var csv = require('fast-csv');
    var team = [];
    var matchId = [];
    var bowlersObject = [];
    var extraRunPerTeam = [];
    return new Promise(function (resolve, reject) {
        fs.createReadStream(matchFile).pipe(csv()).on('data', function (rawData) {
            obj = {};
            if (rawData[1] == 2016 && rawData[1] != "") {
                matchId.push(rawData[0]);
            }
        }).on('end', function () {
            fs.createReadStream(deliveriesFileName).pipe(csv()).on('data', function (rawData) {
                if (matchId.includes(rawData[0])) {
                    team.push(rawData[3])
                    teamBowlerObject = {}
                    teamBowlerObject["bowler_name"] = rawData[3];
                    teamBowlerObject["extra_run"] = rawData[16];
                    bowlersObject.push(teamBowlerObject);
                }
            }).on('end', function () {
                let teamsName = [...new Set(team)];
                for (index = 0; index < teamsName.length; index++) {
                    extraRunsObject = {}
                    let name = teamsName[index];
                    let runs = 0;
                    for (let k = 0; k < bowlersObject.length; k++) {
                        if (name == bowlersObject[k].bowler_name) {
                            runs += Number(bowlersObject[k].extra_run);
                        }
                    }
                    extraRunsObject["y"] = runs;
                    extraRunsObject["label"] = name;
                    extraRunPerTeam.push(extraRunsObject);
                }
                resolve(extraRunPerTeam);
            })
        })
    })
}
getWiningListPerYear('../csv-files/matches.csv', '../csv-files/deliveries.csv');

module.exports = {
    getWiningListPerYear: getWiningListPerYear,
}