function getWiningListPerYear(fileName) {
    var fs = require('fs');
    var csv = require('fast-csv');
    var team = [];
    var winner = [];
    var matchYear = [];
    var listOfObject = [];
    let teamWinPerYear = [];
    return new Promise(function (resolve, reject) {
        fs.createReadStream(fileName).pipe(csv()).on('data', function (rawData) {
            bowlerObject = {};
            if (rawData[10] != "winner" && rawData[10] != "") {
                winner.push(rawData[10]);
            }
            if (rawData[1] != "season" && rawData[1] != "") {
                matchYear.push(rawData[1]);
            }
            if (rawData[0] != 'id') {
                bowlerObject["year"] = rawData[1];
                bowlerObject["winner"] = rawData[10];
                listOfObject.push(bowlerObject);
            }
        }).on('end', function () {
            team = [...new Set(winner)];
            let key = [...new Set(matchYear)].sort();
            let winningTeamPerYear = [];
            for (let h = 0; h < key.length; h++) {
                let season = key[h];
                for (let j = 0; j < team.length; j++) {
                    let matchData = {}
                    let count = 0;
                    let teamItem = team[j];
                    for (var k = 0; k < listOfObject.length; k++) {
                        if (listOfObject[k].year == season) {
                            if (teamItem == listOfObject[k].winner) {
                                count++;
                            }
                        }
                    }
                    matchData['winMatch'] = count;
                    matchData['year'] = season;
                    matchData['teamName'] = teamItem;
                    winningTeamPerYear.push(matchData);
                }
            }
            for (let k = 0; k < team.length; k++) {
                let temp = team[k];
                let winRecord = [];
                let winObject = {};
                for (let r = 0; r < winningTeamPerYear.length; r++) {
                    if (temp == winningTeamPerYear[r].teamName) {
                        winRecord.push(winningTeamPerYear[r].winMatch)
                    }
                }
                winObject['name'] = temp;
                winObject['data'] = winRecord;
                teamWinPerYear.push(winObject);
            }
            resolve(teamWinPerYear);
        });
    })
}
getWiningListPerYear('../csv-files/matches.csv');

function test(winningTeamPerYear, team) {

}
module.exports = {
    getWiningListPerYear: getWiningListPerYear,
}